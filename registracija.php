  <?php include('database.php') ?>
  <!DOCTYPE html>
  <html>
  <head>
    <title>Registracija</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <style type="text/css">
      body{
      background-image: url(upload/background.jpg);
      background-size: cover;
      background-attachment: fixed;}
  </style>
  </head>
  <body>
    <div class="header">
      <h2>Registracija</h2>
    </div>
      
    <form method="post" action="registracija.php">
      <?php include('greske.php'); ?>
      <div class="input-group">
        <label>Korisničko ime</label>
        <input type="text" name="username" value="<?php echo $username; ?>">
      </div>
      <div class="input-group">
        <label>Email adresa</label>
        <input type="email" name="email" value="<?php echo $email; ?>">
      </div>
      <div class="input-group">
        <label>Lozinka</label>
        <input type="password" name="password_1">
      </div>
      <div class="input-group">
        <label>Potvrda lozinke</label>
        <input type="password" name="password_2">
      </div>
      <div class="input-group">
        <button type="submit" class="button" name="reg_user">Pošalji</button>
      </div>
      <p>
          Već imate račun? <a href="index.php">Prijavite se</a>
      </p>
    </form>
  </body>
  </html>