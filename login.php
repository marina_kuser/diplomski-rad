  <?php 
  header("X-XSS-Protection: 0");
    session_start(); 

    if (!isset($_SESSION['username'])) {
      $_SESSION['msg'] = "Morate se prijaviti";
      header('location: index.php');
    }
    if (isset($_GET['logout'])) {
      session_destroy();
      unset($_SESSION['username']);
      header("location: index.php");
    }
  ?>
  <?php include('header.php'); ?>
  <body>
  <?php include('navbar.php'); ?>

  <div class="content">
      <?php if (isset($_SESSION['success'])) : ?>
        <div class="error success" >
          <h3>
            <?php 
              echo $_SESSION['success']; 
              unset($_SESSION['success']);
            ?>
          </h3>
        </div>
      <?php endif ?>

      <?php  if (isset($_SESSION['username'])) : ?>
        <p align="left" style="font-size:150%;">&nbsp&nbspDobrodošli, <strong><?php echo $_SESSION['username']; ?></strong></p>
        <p align="center" style="font-size:170%;">Želite napisati neku poruku?</p>
        
        <form method="post" align="center" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <input type="text" name="name" align="center"><br>
        <input type="submit" name="submit" value="Potvrdi" align="center"> <br>
        <?php
        if(isset($_POST['submit'])) {
          //XSS injection - function htmlspecialchars
            $name = htmlspecialchars($_POST['name']);
            echo "Vaša poruka je: <b> $name </b><br>";
        }
        ?>
      </form>
      <?php endif ?>
  </div>

  <div class="container">
    <h1 class="page-header text-center">JELOVNIK</h1>
    <ul class="nav nav-tabs">
      <?php
        $sql="select * from category order by categoryid asc limit 1";
        $fquery=$conn->query($sql);
        $frow=$fquery->fetch_array();
        ?>
          <li class="active"><a data-toggle="tab" href="#<?php echo $frow['catname'] ?>"><?php echo $frow['catname'] ?></a></li>
        <?php

        $sql="select * from category order by categoryid asc";
        $nquery=$conn->query($sql);
        $num=$nquery->num_rows-1;

        $sql="select * from category order by categoryid asc limit 1, $num";
        $query=$conn->query($sql);
        while($row=$query->fetch_array()){
          ?>
          <li><a data-toggle="tab" href="#<?php echo $row['catname'] ?>"><?php echo $row['catname'] ?></a></li>
          <?php
        }
      ?>
    </ul>

    <div class="tab-content">
      <?php
        $sql="select * from category order by categoryid asc limit 1";
        $fquery=$conn->query($sql);
        $ftrow=$fquery->fetch_array();
        ?>
          <div id="<?php echo $ftrow['catname']; ?>" class="tab-pane fade in active" style="margin-top:20px;">
            <?php

              $sql="select * from product where categoryid='".$ftrow['categoryid']."'";
              $pfquery=$conn->query($sql);
              $inc=4;
              while($pfrow=$pfquery->fetch_array()){
                $inc = ($inc == 4) ? 1 : $inc+1; 
                if($inc == 1) echo "<div class='row'>"; 
                ?>
                  <div class="col-md-3">
                    <div class="panel panel-default">
                      <div class="panel-heading text-center">
                        <b><?php echo $pfrow['productname']; ?></b>
                      </div>
                      <div class="panel-body">
                        <img src="<?php if(empty($pfrow['photo'])){echo "upload/noimage.jpg";} else{echo $pfrow['photo'];} ?>" height="225px;" width="100%">
                      </div>
                      <div class="panel-footer text-center">
                        <?php echo number_format($pfrow['price'], 2); ?> &#107;&#110;
                      </div>
                    </div>
                  </div>
                <?php
                if($inc == 4) echo "</div>";
              }
              if($inc == 1) echo "<div class='col-md-3'></div><div class='col-md-3'></div><div class='col-md-3'></div></div>"; 
              if($inc == 2) echo "<div class='col-md-3'></div><div class='col-md-3'></div></div>"; 
              if($inc == 3) echo "<div class='col-md-3'></div></div>"; 
            ?>
            </div>
        <?php

        $sql="select * from category order by categoryid asc";
        $tquery=$conn->query($sql);
        $tnum=$tquery->num_rows-1;

        $sql="select * from category order by categoryid asc limit 1, $tnum";
        $cquery=$conn->query($sql);
        while($trow=$cquery->fetch_array()){
          ?>
          <div id="<?php echo $trow['catname']; ?>" class="tab-pane fade" style="margin-top:20px;">
            <?php

              $sql="select * from product where categoryid='".$trow['categoryid']."'";
              $pquery=$conn->query($sql);
              $inc=4;
              while($prow=$pquery->fetch_array()){
                $inc = ($inc == 4) ? 1 : $inc+1; 
                if($inc == 1) echo "<div class='row'>"; 
                ?>
                  <div class="col-md-3">
                    <div class="panel panel-default">
                      <div class="panel-heading text-center">
                        <b><?php echo $prow['productname']; ?></b>
                      </div>
                      <div class="panel-body">
                        <img src="<?php if($prow['photo']==''){echo "upload/noimage.jpg";} else{echo $prow['photo'];} ?>" height="225px;" width="100%">
                      </div>
                      <div class="panel-footer text-center">
                           <?php echo number_format($prow['price'], 2); ?> &#107;&#110;
                      </div>
                    </div>
                  </div>
                <?php
                if($inc == 4) echo "</div>";
              }
              if($inc == 1) echo "<div class='col-md-3'></div><div class='col-md-3'></div><div class='col-md-3'></div></div>"; 
              if($inc == 2) echo "<div class='col-md-3'></div><div class='col-md-3'></div></div>"; 
              if($inc == 3) echo "<div class='col-md-3'></div></div>"; 
            ?>
            </div>
          <?php
        }
      ?>
    </div>
    <div align="center">
    <p> <a href="index.php?logout='1'" style="color: red; font-size:170%;"><b>ODJAVA</b></a> </p>
  </div>
  </div>
  </body>
  </html>