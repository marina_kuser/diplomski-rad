  <?php include('database.php') ?>
  <!DOCTYPE html>
  <html>
  <head>
    <title>Narudžba hrane</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <style type="text/css">
      body{
      background-image: url(upload/background.jpg);
      background-size: cover;
      background-attachment: fixed;}
  </style>
  </head>
  <body>
    <h1><b>Prvo se morate prijaviti!</b></h1>
    <style type="text/css">
      h1{
      font-family: Arial, "Times New Roman";
      font-size: 35px;
      font-weight: 200;
      text-align: center;
      color: red;
  </style>
    <div class="header">
      <h2>Prijava</h2>
    </div>
       
    <form method="post" action="index.php">
      <?php include('greske.php'); ?>
      <div class="input-group">
          <label>Korisničko ime</label>
          <input type="text" name="username" >
      </div>
      <div class="input-group">
          <label>Lozinka</label>
          <input type="password" name="password">
      </div>
      <div class="input-group">
          <button type="submit" class="button" name="login_user">Pošalji</button>
      </div>
      <p>
          Nemate račun? <a href="registracija.php">Registrirajte se</a>
      </p>
    </form>
  </body>
  </html>