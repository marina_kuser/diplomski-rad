	<?php include('header.php'); ?>
	<body>
	<?php include('navbar.php'); ?>
	<div class="container">
		<h1 class="page-header text-center">KUPCI</h1>
		<table class="table table-striped table-bordered">
			<thead>
				<th>Datum</th>
				<th>Kupac</th>
				<th>Ukupno</th>
				<th>Detalji</th>
			</thead>
			<tbody>
				<?php 
					$sql="select * from purchase order by purchaseid desc";
					$query=$conn->query($sql);
					while($row=$query->fetch_array()){
						?>
						<tr>
							<td><?php echo date('d M, Y H:i', strtotime($row['date_purchase'])) ?></td>
							<td><?php echo $row['customer']; ?></td>
							<td class="text-right"> <?php echo number_format($row['total'], 2); ?> &#107;&#110;</td>
							<td><a href="#details<?php echo $row['purchaseid']; ?>" data-toggle="modal" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-search"></span> Pogledaj </a>
								<?php include('sales_modal.php'); ?>
							</td>
						</tr>
						<?php
					}
				?>
			</tbody>
		</table>
	</div>
	</body>
	</html>