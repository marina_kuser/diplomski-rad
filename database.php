  <?php
  session_start();

  $username = "";
  $email    = "";
  $errors = array(); 

  $con = mysqli_connect('localhost', 'root', '', 'phplogin');

  if (isset($_POST['reg_user'])) {
    //Escaping string protect database against SQL injection 
    $username = mysqli_real_escape_string($con, $_POST['username']);
    $email = mysqli_real_escape_string($con, $_POST['email']);
    $password_1 = mysqli_real_escape_string($con, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($con, $_POST['password_2']);

    if (empty($username)) { array_push($errors, "Korisničko ime je obavezno"); }
    if (empty($email)) { array_push($errors, "Email adresa je obavezna"); }
    if (empty($password_1)) { array_push($errors, "Lozinka je obavezna"); }
    if ($password_1 != $password_2) {
    array_push($errors, "Lozinke se ne poklapaju!");
    }

    $user_check_query = "SELECT * FROM users WHERE username='$username' OR email='$email' LIMIT 1";
    $result = mysqli_query($con, $user_check_query);
    $user = mysqli_fetch_assoc($result);
    
    if ($user) { 
      if ($user['username'] === $username) {
        array_push($errors, "Korisničko ime već postoji");
      }

      if ($user['email'] === $email) {
        array_push($errors, "Email adresa već postoji");
      }
    }

    if (count($errors) == 0) {
      $password = md5($password_1);

      $sql = "INSERT INTO users (username, email, password) 
            VALUES('$username', '$email', '$password')";
      mysqli_query($con, $sql);
      $_SESSION['username'] = $username;
      $_SESSION['success'] = "";
      header('location: login.php');
    }
  }

  if (isset($_POST['login_user'])) {
    $username = mysqli_real_escape_string($con, $_POST['username']);
    $password = mysqli_real_escape_string($con, $_POST['password']);

    if (empty($username)) {
      array_push($errors, "Korisničko ime je obavezno");
    }
    if (empty($password)) {
      array_push($errors, "Lozinka je obavezna");
    }

    if (count($errors) == 0) {
      $password = md5($password);
      $sql = "SELECT * FROM users WHERE username='$username' AND password='$password'";
      $results = mysqli_query($con, $sql);
      if (mysqli_num_rows($results) == 1) {
        $_SESSION['username'] = $username;
        $_SESSION['success'] = "";
        header('location: login.php');
      }else {
        array_push($errors, "Pogrešna kombinacija korisničkog imena/lozinke");
      }
    }
  }

  ?>